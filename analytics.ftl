<script type="text/javascript">
<#if (streamwize.enableConsole?has_content)>
var count = 0;
var buffer = [];
var timer = null;

function swPushConsoleBuffer() {
	var buf = "";
	for (var i = 0; i < buffer.length; i++)
		buf = buf + buffer[i]
	buffer = [];
	_sw_ajax_2("/controller.jsp?operation=console&contentId=${contentId}&message=" + encodeURIComponent(buf), function() {}, true);
}


function swConsole(str) {
	count++;
	buffer.push(str+"\n");
	if (buffer.length > 100) {
		clearTimeout(timer);
		timer = null;
		swPushConsoleBuffer();
	} else {
		if (timer == null)
			timer = setTimeout(function() {
				swPushConsoleBuffer();
			}, 1000)
	}
}
function takeOverConsole(){
    var console = window.console
    if (!console) return
    function intercept(method){
        var original = console[method]
        console[method] = function(){
        	swConsole(arguments[0]);
            if (original.apply){
                // Do this for normal browsers
                original.apply(console, arguments)
            }else{
                // Do this for IE
                var message = Array.prototype.slice.apply(arguments).join(' ')
                original(message)
            }
        }
    }
    var methods = ['log', 'warn', 'error']
    for (var i = 0; i < methods.length; i++)
        intercept(methods[i])
}
takeOverConsole();

function _sw_ajax_2(url, onDone, synch) {
    var xmlhttp;

    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (onDone) {
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				onDone(xmlhttp.responseText.trim());
			}
		}
    }

    xmlhttp.open("GET", url, synch);
    xmlhttp.send();
}

</#if>
<#if (streamwize.realtimeTemplate?has_content)>
if (!!window.EventSource) {
var eventSource = new EventSource('template?contentType=' + encodeURIComponent('${streamwize.contentType}'));
} else {
	  // Result to xhr polling :(
}
	
eventSource.addEventListener('refresh', function(e) {
	console.log("Receieved real time template update");
	var data = JSON.parse(e.data);
	window.location.reload(true);	// Force browser to go to server and not rely on cache
}, false);

eventSource.addEventListener('open', function(e) {
  // Connection was opened.
}, false);

eventSource.addEventListener('error', function(e) {
  if (e.readyState == EventSource.CLOSED) {
    // Connection was closed.
  }
}, false);

</#if>


function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var topWin = window;
var inCMS = getParameterByName("_sw_suppressFNT") == "true";
try {
	while (topWin.location.href.indexOf("capsuleInner.jsp") >= 0 &&
		topWin.location.href.indexOf("42andpark.com/capsule/") < 0 &&
		topWin.location.href.indexOf("42andpark.com/capsule.jsp") < 0 &&
		topWin != topWin.parent)
			topWin = topWin.parent;
	topWin = topWin.parent;
} catch(e) {
	;
}

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

function hookOnLoad(func) {
    var oldOnLoad = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func
    } else {
        window.onload = function () {
            oldOnLoad();
            func();
        }
    }
}

var reported = false;
function reportToGA(referrer) {
	if (! reported) {
		reported = true;
		ga('create', 'UA-51293010-1', 'auto');
		ga('require', 'displayfeatures');
		ga('set', 'dimension3', '${streamwize.contentRef}');
		ga('set', 'dimension4', referrer);
		ga('set', 'dimension5', '${streamwize.contentType}');
		ga('send', 'pageview', cap);
		if (referrer)
			ga('send', 'event', cap, 'Referrer', referrer);
		<#if (streamwize.ownerTrackingId?has_content) >
		ga('create', '${streamwize.ownerTrackingId}', 'auto', {'name':'ownerTracker'});
		ga('ownerTracker.set', 'dimension4', referrer);
		ga('ownerTracker.require', 'displayfeatures');
		ga('ownerTracker.send', 'pageview', cap);
		if (referrer)
			ga('ownerTracker.send', 'event', cap, 'Referrer', referrer);
		</#if>
	}
}


var started = new Date();
if (! inCMS) {
	var isOnIOS = navigator.userAgent.match(/iPad/i)|| navigator.userAgent.match(/iPhone/i);
	var eventName = isOnIOS ? "pagehide" : "beforeunload";
	
	window.addEventListener(eventName, function (event) {
	    var ended = new Date();
	    var seconds = Math.round( (ended - started) / 1000 );
	    seconds = Math.min(seconds, 60 *60);	// No more than 1 hour allowed
	    ga('set', 'metric2', seconds);
	    ga('send', 'event', cap, 'Measurement', 'On page for ' + seconds + 's', seconds, {transport: 'beacon'});
		<#if (streamwize.ownerTrackingId?has_content) >
	    ga('ownerTracker.set', 'metric2', seconds);
	    ga('ownerTracker.send', 'event', cap, 'Measurement', 'On page for ' + seconds + 's', seconds, {transport: 'beacon'});
		</#if>
	});

	setTimeout(function() {
		if (!reported) {
			console.log("Timed out hearing back from parent frame so self reporting to GA");
			var referrer = null;
			try {
				referrer = top.location.href;
			} catch (e) {
				
			}
			reportToGA(referrer);
		}
	}, 5000);

	<#if (streamwize.caption?has_content) >
	var cap = '${streamwize.caption?js_string} ${streamwize.contentRef}';
	<#elseif (caption?has_content)>
	var cap = '${caption?js_string} ${streamwize.contentRef}';
	<#elseif (name?has_content)>
	var cap = '${name?js_string} ${streamwize.contentRef}';
	<#else>
	var cap = 'unnamed ${streamwize.contentRef}';
	</#if>
	
	function messageListenerAnal(event){
	    var data = event.data;
	    if (typeof(data) === "string"){
	    	try {
	            data = JSON.parse(data.trim()); 
	    	}
	    	catch (e) {
	    		//console.log("non JSON message", data);
	    	}
	    }
	    if (data && data.action && data.action == 'returnMeasure') {
	    	reportToGA(data.referrer);
	    }
	}
	
	if (window.addEventListener){
	  addEventListener("message", messageListenerAnal, false);
	} else {
	  attachEvent("onmessage", messageListenerAnal);
	}
	
	setTimeout(function(){
		topWin.postMessage(
				 JSON.stringify({action: 'removePreload', cid: '${streamwize.contentRef}' }),
		         '*' //destination domain 
		);
	}, 1000);
	
	
	<#if (streamwize.adUnitWidth?has_content && streamwize.adUnitHeight?has_content)>
	topWin.postMessage(
			JSON.stringify({action: 'setAdUnitSize', cid: '${streamwize.contentRef}', width: '${(streamwize.adUnitWidth)!""}', height: '${(streamwize.adUnitHeight)!""}' }),
	         '*' //destination domain 
	);
	</#if>
	hookOnLoad(function(){
		topWin.postMessage(
		         JSON.stringify({action: 'measure', cid: '${streamwize.contentRef}', elemId: '_sw_adunit', minPercent: '${streamwize.measurementPercentage!""}',
		         minSecs: '${streamwize.measurementSeconds!""}', caption: cap, ownerId: '${streamwize.ownerTrackingId!""}' }),
		         '*' //destination domain 
		    );
	});
} // if not in CMS
</script>
