function getStyleBySelector( selector )
{
    var sheetList = document.styleSheets;
    var ruleList;
    var i, j;

    /* look through stylesheets in reverse order that
       they appear in the document */
    for (i=sheetList.length-1; i >= 0; i--)
    {
        ruleList = sheetList[i].cssRules;
        if (ruleList) for (j=0; j<ruleList.length; j++)
        {
            if (ruleList[j].type == CSSRule.STYLE_RULE && 
                ruleList[j].selectorText == selector)
            {
                return ruleList[j].style;
            };   
        };
    };
    return null;	
};

function dropScroller()
{
	$("#shoppingScroller").css("bottom", "-200px");
	$("#shoppingScroller").css("opacity", "0.70");
};

function showScroller()
{
	$("#shoppingScroller").css("bottom", "-0px");
	$("#shoppingScroller").css("opacity", "1.0");
};

