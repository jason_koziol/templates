

function popCapsuleParent(event){
   try{
    <#if streamwize.frameBusterURL?has_content>
  var iframe = document.createElement("iframe");
  iframe.src = "${streamwize.frameBusterURL}?contentId=" + event.data.cid;
  iframe.width = "0";
  iframe.height = "0";
  iframe.frameBorder = "0";
  document.getElementById('body').appendChild(iframe);
    <#else>
        if( window.parent.document.referrer != "" ){
            var protocol = window.parent.document.referrer.split('://')[0];
            var domain = window.parent.document.referrer.split('://')[1];
            domain = domain.split('/')[0];
            destinationDomain = protocol + "://" + domain;             
        }
         window.parent.parent.postMessage(
            { cid: event.data.cid, h: '${streamwize.lightboxHeight!""}', w: '${streamwize.lightboxWidth!""}' },
             destinationDomain //destination domain 
        );
        window.parent.parent.parent.postMessage(
            { cid: event.data.cid, h: '${streamwize.lightboxHeight!""}', w: '${streamwize.lightboxWidth!""}' },
             "http://demogod.42andpark.com" //destination domain 
        );
     </#if>
   }
    catch(err){
        console.log("Error popping capsule ", err);
     }

    return false;
}
